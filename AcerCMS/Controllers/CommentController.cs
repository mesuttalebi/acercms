﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using AcerCMS.Filters;
using AcerCMS.Models;
using AcerCMS.ModelService;

namespace AcerCMS.Controllers
{
    public class CommentController : Controller
    {
        //// GET: Comment
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //// GET: Comment/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Comment/Create
        //[ChildActionOnly]
        [MergeModelState]
        [ShowMessage]
        public ActionResult Create(int entityId, EntityType entity)
        {            
            var model = CommentModelService.CreateCommentViewModel(entityId, entity);
            return PartialView("_CommentFormPartial", model);
        }

        // POST: Comment/Create
        [HttpPost]
        public ActionResult Create(Comment comment)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Lütfen bilgileri kontrol ederek tekrar deneyiniz!");
                TempData["ModelState"] = ModelState;
                return RedirectToAction("Create", new {entityId = comment.EntityId, entity = comment.Entity});
            }

            try
            {
                CommentModelService.Save(comment);
                TempData["Message"] = "Tebrikler! Kayıt Başarıyla Eklenmiştir!";
            }
            catch (Exception ex)
            {
                //Log Error;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                TempData["Message"] = "HATA! Kayıt Eklenemedi!";                
            }

            return RedirectToAction("GetComments", new { entityId = comment.EntityId, entity = comment.Entity });
        }

        public ActionResult GetComments(int entityId, EntityType entity)
        {
            var model = CommentModelService.GetComments(entityId, entity);
            return PartialView("_CommentsPartial", model);
        }

        // GET: Comment/Edit/5s        
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Comment/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Comment/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Comment/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
